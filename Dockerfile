FROM alpine:3.12

ENV ANSIBLE_HOST_KEY_CHECKING="False" \
    SSH_KEYDIR="/root/.ssh"

RUN apk update && \
    apk add --no-cache \
    ansible \
    openssh \
 && rm -rf /var/cache/apk/* && \
    ansible --version

ENTRYPOINT []
