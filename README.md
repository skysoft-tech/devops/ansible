# Ansible

Docker image with Ansible config management tool.

[![pipeline status](https://gitlab.com/skysoft-tech/devops/ansible/badges/develop/pipeline.svg)](https://gitlab.com/skysoft-tech/devops/ansible/commits/develop)

## Tags

| Tag      | Ansible |
| -------- | ------- |
| `2.6.4`  | 2.6.4   |
| `2.9.9`  | 2.9.9   |